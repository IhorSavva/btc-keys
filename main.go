package main

import (
	"encoding/hex"
	"fmt"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcutil"
	"github.com/btcsuite/btcutil/hdkeychain"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"strings"
)

var (
	DashMainnetParams = &chaincfg.Params{
		// Address encoding magics
		PubKeyHashAddrID: 0x4c,
		ScriptHashAddrID: 0x10,
		PrivateKeyID:     0xcc,
		HDPrivateKeyID:   [4]byte{0x04, 0x88, 0xad, 0xe4},
		HDPublicKeyID:    [4]byte{0x04, 0x88, 0xb2, 0x1e},
	}

	// TODO Dash testnet
)

func init() {
	chaincfg.Register(DashMainnetParams)
	// TODO
	//chaincfg.Register(DashTestnetParams)
}

func main() {
	//args := os.Args[1:]
	//if len(args) < 1 {
	//	fmt.Println("Error: need private key parameter.")
	//	return
	//}
	//priv := args[0]
	//
	////err := describePrivateKeysHex(args)
	////err := describeWIF(priv)
	//err := describeExtendedPrivate(priv, 1000)
	//if err != nil {
	//	fmt.Println(err.Error())
	//	return
	//}

	describeP2SHAddress("2N5vbPQB7pMqyVa8JRcpZdCQLQCwDZCMyfo", &chaincfg.TestNet3Params)
}

func describeP2SHAddress(p2shAddress string, params *chaincfg.Params) error {
	addr, err := btcutil.DecodeAddress(p2shAddress, params)
	if err != nil {
		return errors.Wrap(err, "Failed to decode the address according to provided NetParams")
	}

	printResult := map[string]string {
		"Address bytes for the TXOut's Script, the ScriptAddress (hex)": hex.EncodeToString(addr.ScriptAddress()),
	}

	bb, err := txscript.NewScriptBuilder().AddOp(txscript.OP_HASH160).AddData(addr.ScriptAddress()).AddOp(txscript.OP_EQUAL).Script()
	if err != nil {
		return errors.Wrap(err, "Failed to build bytes of the Script", logan.F{
			"address": addr,
		})
	}

	printResult["TXOut's Script P2SH (hex)"] = hex.EncodeToString(bb)

	printMap(printResult)
	return nil
}

func describeExtendedPrivate(extPrivKey string, n int, params *chaincfg.Params) error {
	if n >= hdkeychain.HardenedKeyStart {
		return fmt.Errorf("n cannot be equal or more than %d", hdkeychain.HardenedKeyStart)
	}

	extKey, err := hdkeychain.NewKeyFromString(extPrivKey)
	if err != nil {
		return errors.Wrap(err, "Failed to create new extended private key from the string representation")
	}

	extKey.SetNet(params)

	for i := 0; i <= n; i++ {
		child, err := extKey.Child(uint32(i))
		if err != nil {
			return errors.Wrap(err, "Failed to derive Child", logan.F{
				"child_i": i,
			})
		}

		priv, err := child.ECPrivKey()
		if err != nil {
			return errors.Wrap(err, "Failed to get PrivKey from the Child", logan.F{
				"child_i": i,
			})
		}

		fmt.Println(i)
		describePrivateKey(priv, params)
		fmt.Println("==============================================================================================")
	}

	return nil
}

func describeWIFPrivateKey(wifPrivateKey string) error {
	wif, err := btcutil.DecodeWIF(wifPrivateKey)
	if err != nil {
		return errors.Wrap(err, "Failed to decode WIF from string representation")
	}

	describePrivateKey(wif.PrivKey, nil)
	return nil
}

func describePrivateKeysHex(privHexes []string) error {
	for _, privKey := range privHexes {
		err := describePrivateKeyHex(privKey)
		if err != nil {
			return err
		}

		fmt.Println("==============================================================================================")
	}

	return nil
}

func describePrivateKeyHex(privHex string) error {
	bb, err := hex.DecodeString(privHex)
	if err != nil {
		return fmt.Errorf("Could not parse hex to bytes: '%s'.\n", privHex)
	}

	if len(bb) != 32 {
		return fmt.Errorf("PrivateKey length is not 32 bytes, but %d: '%v'.\n", len(bb), bb)
	}

	priv, _ := btcec.PrivKeyFromBytes(btcec.S256(), bb)
	describePrivateKey(priv, nil)
	return nil
}

// DescribePrivateKey accepts nil params.
func describePrivateKey(privKey *btcec.PrivateKey, params *chaincfg.Params) {
	priv := privKey.Serialize()
	fmt.Printf("PrivateKey (%d B, hex):                   %x\n", len(priv), priv)

	pub := privKey.PubKey().SerializeCompressed()
	fmt.Printf("Comprssed PublicKey (%d B, hex):          %x\n", len(pub), pub)

	hash160 := btcutil.Hash160(pub)
	fmt.Printf("Hash160 of PublicKey (%d B, hex):         %x\n", len(hash160), hash160)

	fmt.Printf("Pay-to-pubkey-hash script:                OP_DUP OP_HASH160 %x OP_EQUALVERIFY OP_CHECKSIG\n", hash160)

	addr, _ := btcutil.NewAddressPubKeyHash(hash160, &chaincfg.MainNetParams)
	script, _ := txscript.PayToAddrScript(addr)
	fmt.Printf("Pay-to-pubkey-hash script (%d B, hex):    %x\n", len(script), script)

	// Params specific
	if params != nil {
		printParamsSpecific(privKey, privKey.PubKey(), params)
	} else {
		fmt.Println("---Bitcoin Mainnet:")
		printParamsSpecific(privKey, privKey.PubKey(), &chaincfg.MainNetParams)

		fmt.Println("---Bitcoin Testnet:")
		printParamsSpecific(privKey, privKey.PubKey(), &chaincfg.TestNet3Params)

		// TODO
		//fmt.Println("---Dash Mainnet:")
	}
}

func printParamsSpecific(priv *btcec.PrivateKey, pub *btcec.PublicKey, params *chaincfg.Params) {
	// Ignoring error, as it's only returned when Params is nil.
	wif, _ := btcutil.NewWIF(priv, params, true)
	fmt.Printf("PrivateKey in Wallet Import Format (WIF): %s\n", wif.String())

	addr, _ := btcutil.NewAddressPubKeyHash(btcutil.Hash160(pub.SerializeCompressed()), params)
	fmt.Printf("Address (pubkey-hash, base58):            %s\n", addr.String())
}

func printMap(m map[string]string) {
	var maxLenKey int
	for k, _ := range m {
		if len(k) > maxLenKey {
			maxLenKey = len(k)
		}
	}

	for k, v := range m {
		fmt.Println(k + ": " + strings.Repeat(" ", maxLenKey - len(k)) + v)
	}
}